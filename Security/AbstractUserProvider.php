<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Security;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class AbstractUserProvider.
 */
abstract class AbstractUserProvider implements UserProviderInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $username
     *
     * @return UserInterface
     */
    public function loadUserByUsername($username)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository($this->getUserClass())->createQueryBuilder('u');
        $qb->where('lower(u.email) = :email')
            ->setParameter('email', mb_strtolower($username, 'UTF-8'));
        $result = $qb->setMaxResults(1)->getQuery()->execute();

        $user = null;
        if (count($result) > 0) {
            $user = current($result);
        }

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        } elseif ($user->getEmail() !== mb_strtolower($username, 'UTF-8')) {
            $user->setEmail(mb_strtolower($username, 'UTF-8'));
            $this->em->flush();
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        $refreshedUser = $this->em->getRepository($this->getUserClass())->find($user->getId());
        if (null === $refreshedUser) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', json_encode($user->getId())));
        }

        return $refreshedUser;
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === $this->getUserClass();
    }

    /**
     * @return string
     */
    abstract protected function getUserClass();
}
