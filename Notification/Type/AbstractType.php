<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification\Type;

use Datatourisme\Bundle\WebAppBundle\Notification\NotificationTypeInterface;
use Monolog\Logger;

/**
 * Class AbstractType.
 */
abstract class AbstractType implements NotificationTypeInterface
{
    protected $subject;

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    abstract public function getMessage();

    public function getLevel()
    {
        return Logger::NOTICE;
    }

    public function getContext()
    {
        return array();
    }

    public function getDescription()
    {
        return null;
    }

    public function getRoute()
    {
        return null;
    }

    public function getRouteTitle()
    {
        return null;
    }

    public function getRouteParameters()
    {
        return [];
    }

    public function getRecipients()
    {
        return null;
    }

    public function getExcludedRecipients()
    {
        return null;
    }

    public function getEntity()
    {
        return null;
    }
}
