<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Twig\Markdown;

use cebe\markdown\block\TableTrait;
use cebe\markdown\GithubMarkdown;

/**
 * Class DatatourismeMarkdown.
 */
class DatatourismeMarkdown extends GithubMarkdown
{
    use TableTrait;
    use MarkdownHighlightTrait;

    /**
     * {@inheritdoc}
     *
     * Overrinding parent's method in order to add some specific styles.
     */
    protected function renderTable($block)
    {
        $content = '';
        $this->_tableCellAlign = $block['cols'];
        $content .= "<thead>\n";
        $first = true;
        foreach ($block['rows'] as $row) {
            $this->_tableCellTag = $first ? 'th' : 'td';
            $align = empty($this->_tableCellAlign[$this->_tableCellCount])
                   ? ''
                   : ' align="'.$this->_tableCellAlign[$this->_tableCellCount++].'"';
            $tds = "<$this->_tableCellTag$align>".
                   trim($this->renderAbsy($this->parseInline($row))).
                   "</$this->_tableCellTag>";
            $content .= "<tr>$tds</tr>\n";
            if ($first) {
                $content .= "</thead>\n<tbody>\n";
            }
            $first = false;
            $this->_tableCellCount = 0;
        }

        return "<table class=\"table table-responsive m-0\">\n$content</tbody>\n</table>\n";
    }
}
