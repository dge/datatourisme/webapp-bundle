<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\Tests\Validator\Constraints;

use Datatourisme\Bundle\WebAppBundle\Validator\Constraints\StrongPassword;
use Datatourisme\Bundle\WebAppBundle\Validator\Constraints\StrongPasswordValidator;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class StrongPasswordValidatorTest extends ConstraintValidatorTestCase
{
    public $oldPasswords = array(
        "@}ILKG5m%S'/WE}O",
        '4K68o{xbYM%|0ua}',
    );

    protected function createValidator()
    {
        $encoder = new BCryptPasswordEncoder(4);

        // mock UserInterface
        $user = $this->getMockBuilder('Symfony\Component\Security\Core\User\UserInterface')
            ->setMethods(array('getFirstname', 'getLastname', 'eraseCredentials', 'getUsername', 'getSalt', 'getRoles', 'getPassword', 'getPreviousPasswords'))
            ->getMock();
        $user->method('getRoles')->willReturn(array());
        $user->method('getLastname')->willReturn('Famille');
        $user->method('getFirstname')->willReturn('Prénom');
        $user->method('getPreviousPasswords')->willReturn(array_map(function ($password) use ($encoder, $user) {
            return $encoder->encodePassword($password, $user->getSalt());
        }, $this->oldPasswords));

        // mock TokenStorageInterface
        $token = new UsernamePasswordToken($user, $user->getPassword(), 'main_firewall', $user->getRoles());
        $storage = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface')
          ->setMethods(array())
          ->getMock();
        $storage->method('getToken')->willReturn($token);

        // mock EncoderFactoryInterface
        $factory = $this->getMockBuilder('Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface')
          ->setMethods(array())
          ->getMock();
        $factory->method('getEncoder')->willReturn($encoder);

        return new StrongPasswordValidator($storage, $factory);
    }

    /**
     * @dataProvider getValidPasswords
     */
    public function testValidPassword($password)
    {
        $this->validator->validate($password, new StrongPassword());
        $this->assertNoViolation();
    }

    public function getValidPasswords()
    {
        return array(
            array('pt97?uB8eSB6r9E'),
            array('Y6q4-&$eWbs*vZF'),
            array('jMhW@#J=h@-7zQ$'),
        );
    }

    /**
     * @dataProvider getInvalidPasswords
     */
    public function testInvalidPassword($email, $violation)
    {
        $constraint = new StrongPassword();

        $this->validator->validate($email, $constraint);
        $this->assertSame(1, $violationsCount = count($this->context->getViolations()), sprintf('1 violation expected. Got %u.', $violationsCount));
        $this->assertEquals($violation, $this->context->getViolations()->get(0)->getCode());
    }

    public function getInvalidPasswords()
    {
        return array(
            array(null, StrongPassword::TOO_SHORT),
            array('', StrongPassword::TOO_SHORT),
            array("j@'NR*\"`Sn+h5", StrongPassword::TOO_SHORT),
            array('qejIonqySaGAFLMd', StrongPassword::TOO_WEAK),
            array('KWA5QGFJNXO7FTMV', StrongPassword::TOO_WEAK),
            array("I]$,HV^*'+FTWZRL", StrongPassword::TOO_WEAK),
            array('9-4/9=;,)?]^-#/8', StrongPassword::TOO_WEAK),
            array(']k/|);h%{%awkgfe', StrongPassword::TOO_WEAK),
            array($this->oldPasswords[0], StrongPassword::CONTAINS_OLD_PASSWORD),
            array('A5wfamilleA4Jktl7PNSwxP', StrongPassword::CONTAINS_FORBIDDEN_PROPERTIES),
            array('A5wPrenomA4Jktl7PNSwxP', StrongPassword::CONTAINS_FORBIDDEN_PROPERTIES),
        );
    }
}
