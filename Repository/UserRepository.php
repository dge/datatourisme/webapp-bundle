<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository.
 */
class UserRepository extends EntityRepository
{
    private $map;

    /**
     * @param mixed $hierarchy
     */
    public function setHierarchy($hierarchy)
    {
        $this->map = $this->buildRoleMap($hierarchy);
    }

    /**
     * Return all users for a given role.
     *
     * @param string $role
     *
     * @return array
     */
    public function findByRole($role)
    {
        $adminRoles = array();
        foreach ($this->map as $key => $roles) {
            if (in_array($role, $roles)) {
                $adminRoles[] = $key;
            }
        }

        return $this->findBy(array('role' => $adminRoles));
    }

    /**
     * @param $hierarchy
     *
     * @return array
     */
    protected function buildRoleMap($hierarchy)
    {
        $map = array();
        foreach ($hierarchy as $main => $roles) {
            $map[$main] = $roles;
            $visited = array();
            $additionalRoles = $roles;
            while ($role = array_shift($additionalRoles)) {
                if (!isset($hierarchy[$role])) {
                    continue;
                }

                $visited[] = $role;

                foreach ($hierarchy[$role] as $roleToAdd) {
                    $map[$main][] = $roleToAdd;
                }

                foreach (array_diff($hierarchy[$role], $visited) as $additionalRole) {
                    $additionalRoles[] = $additionalRole;
                }
            }

            $map[$main] = array_unique($map[$main]);
        }

        return $map;
    }
}
