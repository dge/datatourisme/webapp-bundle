<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Test\Form\Type;

use Symfony\Component\Form\Tests\Extension\Core\Type\BaseTypeTest;

class HoneypotTypeTest extends BaseTypeTest
{
    const TESTED_TYPE = 'Datatourisme\Bundle\WebAppBundle\Form\HoneypotType';

    public function testSubmitNull($expected = null, $norm = null, $view = null)
    {
        parent::testSubmitNull($expected, $norm, '');
    }

    public function testSubmitFilled()
    {
        $form = $this->factory->createNamed('form')
          ->add('honey', static::TESTED_TYPE);
        $form->submit(array('honey' => 'test'));
        $this->assertFalse($form->isValid());
        $this->assertEquals(1, $form->getErrors()->count());
    }
}
