<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Twig\Markdown;

/**
 * MarkdownHighlightTrait provides code highlighting functionality for Markdown Parsers.
 *
 * @since 2.1.1
 * @author Carsten Brandt <mail@cebe.cc>
 */
trait MarkdownHighlightTrait
{
    /**
     * @var Highlighter
     */
    private static $highlighter;
    /**
     * @inheritdoc
     */
    protected function renderCode($block)
    {
        $block['content'] = htmlspecialchars_decode($block['content']);

        if(class_exists("Highlight\\Highlighter"))  {
            if (self::$highlighter === null) {
                self::$highlighter = new \Highlight\Highlighter();
                self::$highlighter->setAutodetectLanguages([
                    'xml', 'xquery'
                ]);
            }

            try {
                if (isset($block['language'])) {
                    $result = self::$highlighter->highlight($block['language'], $block['content'] . "\n");
                    return "<pre><code class=\"hljs {$result->language} language-{$block['language']}\">{$result->value}</code></pre>\n";
                } else {
                    $result = self::$highlighter->highlightAuto($block['content'] . "\n");
                    return "<pre><code class=\"hljs {$result->language}\">{$result->value}</code></pre>\n";
                }
            } catch (\DomainException $e) {
                return parent::renderCode($block);
            }
        }
        return parent::renderCode($block);
    }
}