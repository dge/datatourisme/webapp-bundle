<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification\Handler;

use Datatourisme\Bundle\WebAppBundle\Mailer\Mailer;
use Datatourisme\Bundle\WebAppBundle\Notification\NotificationTypeInterface;
use Monolog\Handler\AbstractProcessingHandler;

/**
 * Class MailHandler.
 */
class MailerHandler extends AbstractProcessingHandler
{
    /** @var Mailer */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    protected function write(array $record)
    {
        $context = $record['context'];
        if (!isset($context['notification'])) {
            return false;
        }

        /** @var NotificationTypeInterface $notification */
        $notification = $context['notification'];

        // get recipients
        $recipients = $notification->getRecipients();

        // send mail
        if (!empty($recipients)) {
            $this->mailer->send('notification', $recipients, $record);
        }
    }
}
