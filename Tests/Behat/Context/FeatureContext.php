<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Tests\Behat\Context;

use Behat\Behat\Definition\Call\Then;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Mink\Exception\ResponseTextException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;
use Exception;

class FeatureContext extends MinkContext implements KernelAwareContext
{
    private $kernel;

    /**
     * @var bool
     */
    private $javascript = false;

    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @param BeforeScenarioScope $scope
     */
    public function beforeJavascriptScenario(BeforeScenarioScope $scope)
    {
        $this->javascript = true;
    }

    /**
     * @param AfterScenarioScope $scope
     */
    public function afterJavascriptScenario(AfterScenarioScope $scope)
    {
        $this->javascript = false;
    }

    /**
     * @BeforeScenario
     *
     * @param BeforeScenarioScope $scope
     */
    public function beforeScenario(BeforeScenarioScope $scope)
    {
        /*
         * Tricky fix for some KNPPaginator bias:
         * KNPPaginator options set manually $_GET array, which leads to unexpected variable keeping between tests.
         */
        $this->icleanGetGlobals();
    }

    //    /**
    //     * @AfterStep
    //     *
    //     * @param AfterStepScope $scope
    //     * @throws Exception
    //     */
    //    public function afterStep(AfterStepScope $scope)
    //    {
    //        if ($this->javascript) {
    ////            dump($scope->getStep()->getText());
    ////            dump($scope->getStep()->getNodeType());
    ////            die;
    //            if (!$scope->getTestResult()->isPassed()) {
    //                $wait = 40;
    //                try {
    //                    $this->spin(
    //                        function () use (&$scope) {
    //                            return new Then(
    //                                $scope->getStep()->getText(),
    //                                $scope->getStep()->getNodeType()
    //                            );
    //                        },
    //                        $wait
    //                    );
    //                } catch (\Exception $e) {
    //                    throw new \Exception('Failed after '.$wait.' seconds. Error - '.$e->getMessage());
    //                }
    //            }
    //        }
    //    }

    /**
     * @Given /^I wait for (\d+) seconds$/
     */
    public function iWaitForSeconds($seconds)
    {
        sleep($seconds);
    }

    /**
     * @When I click the :arg1 element
     */
    public function iClickTheElement($selector)
    {
        $page = $this->getSession()->getPage();
        $element = $page->find('css', $selector);

        if (empty($element)) {
            throw new Exception("No html element found for the selector ('$selector')");
        }

        $element->click();
    }

    /**
     * @Given I am logged in as :arg1 with :arg2
     */
    public function iAmLoggedInAsWith($arg1, $arg2)
    {
        $this->icleanGetGlobals();
        parent::visit('/');
        parent::fillField('_username', $arg1);
        parent::fillField('_password', $arg2);
        parent::pressButton('Connexion');
    }

    /**
     * @Then I disconnect
     */
    public function iDisconnect()
    {
        parent::visit('/logout');

        $this->iWaitForTextToAppear('Connexion', 1);
        parent::assertPageAddress('/login');
    }

    /**
     * @When I wait for :text to appear while :second seconds
     * @Then I should see :text appear
     *
     * @param $text
     * @param $second
     *
     * @throws Exception
     */
    public function iWaitForTextToAppear($text, $second)
    {
        $this->spin(function (FeatureContext $context) use ($text) {
            try {
                $context->assertPageContainsText($text);

                return true;
            } catch (ResponseTextException $e) {
            }

            return false;
        }, $second);
    }

    /**
     * Based on Behat's own example.
     *
     * @see http://docs.behat.org/en/v2.5/cookbook/using_spin_functions.html#adding-a-timeout
     *
     * @param $lambda
     * @param int $wait
     *
     * @throws \Exception
     */
    public function spin($lambda, $wait = 60)
    {
        $time = time();
        $stopTime = $time + $wait;
        while (time() < $stopTime) {
            try {
                if ($lambda($this)) {
                    usleep(250000);

                    return;
                }
            } catch (\Exception $e) {
                // do nothing
            }

            usleep(250000);
        }

        throw new \Exception("Spin function timed out after {$wait} seconds");
    }

    /**
     * @When /^(?:I) go to "(?P<route>[^"]*)" with random "(?P<class>[^"]*)" id/
     *
     * @param string $route
     * @param string $class
     */
    public function iCrawlRandomId(string $route, string $class)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $entities = $em->getRepository('AppBundle:'.$class)->findAll();
        $entity = $entities[array_rand($entities)];

        $url = $route.'/'.$entity->getId();

        parent::visit($url);
        parent::assertPageAddress($url);

        return;
    }

    /**
     * @When /^(?:I) go relative to "(?P<route>[^"]*)"/
     *
     * @param string $route
     */
    public function iGoRelativeTo(string $route)
    {
        $url = $this->getSession()->getCurrentUrl().$route;

        parent::visit($url);
        parent::assertPageAddress($url);

        return;
    }

    /**
     * @When /^(?:I )submit the "(?P<selector>[^"]*)" form/
     *
     * @param $selector
     *
     * @throws Exception
     */
    public function iSubmitTheForm(string $selector)
    {
        $page = $this->getSession()->getPage();
        $element = $page->find('css', $selector);

        if (empty($element)) {
            throw new Exception("No form found for the selector ('$selector')");
        }

        $element->submit();
    }

    /**
     * Example: And the response content type should be text/csv.
     *
     * @param $content_type
     *
     * @Then /^the response content type should be "(?P<content_type>(?:[^"]|\\")*)"$/
     */
    public function assertResponseContentType($content_type)
    {
        $this->assertSession()->responseHeaderContains('Content-Type', $content_type);
    }

    /**
     * @Then /^(?:the) response headers should contain "(?P<header>[^"]*)":"(?P<value>[^"]*)"/
     *
     * @param $header
     * @param $value
     *
     * @return bool
     *
     * @throws Exception
     */
    public function theHeaderShouldContain(string $header, string $value): bool
    {
        if ($this->getSession()->getResponseHeader($header) !== $value) {
            throw new \Exception('Failed asserting the header '.$header.' contains '.$value.'.');
        }

        return true;
    }

    /**
     * @Then /^(?:the) response body should contain valid JSON/
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function assertResponseContainsValidJson(): bool
    {
        if (null === json_decode($this->getSession()->getPage()->getContent())) {
            throw new \Exception('Failed asserting the response contains valid JSON. Error message: "'.json_last_error_msg().'".');
        }

        return true;
    }

    /**
     * @Then /^(?:I )clean up the GET globals/
     *
     * Tricky fix for some KNPPaginator bias:
     * KNPPaginator options set manually $_GET array, which leads to unexpected variable keeping between tests.
     */
    public function icleanGetGlobals()
    {
        $_GET = array();
    }

    /**
     * @Then /^(?:I )clean up the cache directory/
     */
    public function iCleanCache()
    {
        echo exec('php bin/console cache:clear');
    }

    /**
     * @Then /^(?:I )clean up the data directory/
     */
    public function iClearDataSource(): bool
    {
        $deleteRecursive = function (string $path) use (&$deleteRecursive): bool {
            if (is_dir($path)) {
                $files = array_diff(scandir($path), array('.', '..'));
                foreach ($files as $file) {
                    $deleteRecursive(realpath($path).DIRECTORY_SEPARATOR.$file);
                }

                return rmdir($path);
            } elseif (is_file($path)) {
                return unlink($path);
            }

            return false;
        };

        return $deleteRecursive(
            $this->getContainer()->getParameter('datatourisme_worker')['params']['datatourisme.data.path']
        );
    }

    /**
     * @Then /^(?:I) select "(?P<selector>[^"]*)":"(?P<value>[^"]*)"/
     *
     * @param string $selector
     * @param string $value
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function iSelectField(string $selector, string $value): bool
    {
        $field = $this->getSession()->getPage()->find('css', $selector);
        if (empty($field)) {
            throw new \Exception("No html element found for the selector ('$selector')");
        }

        $field->selectOption($value);

        return true;
    }
}
