<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager;

use Leezy\PheanstalkBundle\Proxy\PheanstalkProxy;

/**
 * Class BeanStalkMessenger.
 */
class BeanStalkMessenger extends PheanstalkProxy
{
    /**
     * @var string
     */
    protected $tube;

    /**
     * @var array
     */
    protected $payload;

    /**
     * @param string $tube
     * @param array  $payload
     * @param int    $priority
     *
     * @return int The new job ID
     */
    public function sendMessage($tube, $payload = null, $priority = PheanstalkProxy::DEFAULT_PRIORITY)
    {
        if (is_array($payload)) {
            $payload = json_encode($payload, JSON_UNESCAPED_SLASHES);
        }

        return $this->putInTube($tube, $payload, $priority);
    }


    /**
     * is the job alive ?
     *
     * @param $id
     *
     * @return int new job id
     */
    public function isAlive($id)
    {
        try {
            $stats = $this->statsJob($id);
        } catch (\Exception $e) {
            return false;
        }
        return $stats['state'] != 'buried';
    }
}
