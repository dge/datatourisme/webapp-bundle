<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Form;

use Datatourisme\Bundle\WebAppBundle\Validator\Constraints\StrongPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class StrongPasswordType extends AbstractType
{
    public function getParent()
    {
        return RepeatedType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(array(
                'user' => null,
                'type' => PasswordType::class,
                'label' => 'label.password',
                'first_options' => array(
                    'label' => 'label.new_password',
                ),
                'second_options' => array(
                    'label' => 'label.repeat_new_password',
                ),
                'invalid_message' => 'non_matching_passwords',
                'constraints' => function (Options $options) {
                    return array(
                        new NotBlank(array('message' => 'empty_password')),
                        new StrongPassword(array(
                            'user' => $options['user'],
                            'shortMessage' => 'password_too_short',
                            'longMessage' => 'password_too_long',
                            'weakMessage' => 'password_too_weak',
                            'forbiddenMessage' => 'password_forbidden',
                            'oldPasswordMessage' => 'password_not_same_as_old',
                        )),
                    );
                },
            ))
        ;
    }
}
