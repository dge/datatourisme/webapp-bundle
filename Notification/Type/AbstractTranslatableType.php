<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification\Type;

use Datatourisme\Bundle\WebAppBundle\Notification\NotificationTypeInterface;
use Monolog\Logger;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractType.
 */
abstract class AbstractTranslatableType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
}
