<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Notification;

use Datatourisme\Bundle\WebAppBundle\Mailer\MailerRecipientInterface;
use Datatourisme\Bundle\WebAppBundle\Notification\ORM\NotificationEntityInterface;

/**
 * Interface NotificationTypeInterface.
 */
interface NotificationTypeInterface
{
    /**
     * @param $subject
     */
    public function setSubject($subject);

    /**
     * @return mixed
     */
    public function getSubject();

    /**
     * @return array
     */
    public function getContext();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getRoute();

    /**
     * @return string
     */
    public function getRouteTitle();

    /**
     * @return array
     */
    public function getRouteParameters();

    /**
     * @return int
     */
    public function getLevel();

    /**
     * @return MailerRecipientInterface[]
     */
    public function getRecipients();

    /**
     * @return MailerRecipientInterface[]
     */
    public function getExcludedRecipients();

    /**
     * @return NotificationEntityInterface
     */
    public function getEntity();
}
