/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

'use strict';

var $ = require("jquery");

$(function () {
    $.fn.initComponents.add(function(dom) {
        $('[data-ajax-content]', dom).each(function () {
            var url = $(this).data('href');
            var container = $(this);
            container.addClass('ajax-loading');
            container.removeClass('ajax-processed');
            $.get(url, function (data) {
                var content = $(data);
                container.html(content);
                content.initComponents();
                setTimeout(function() {
                    // add delay to handle css3 transition
                    container.removeClass('ajax-loading');
                    container.addClass('ajax-processed');
                },50);
            });
        });
    });
});