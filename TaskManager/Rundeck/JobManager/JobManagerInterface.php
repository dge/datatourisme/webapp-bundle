<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\JobManager;

/**
 * Interface JobManagerInterface.
 */
interface JobManagerInterface
{
    /**
     * @param $groupId
     * @param $name
     * @param $scriptUrl
     * @param int         $hour
     * @param string|null $description
     * @param bool        $enable
     */
    public function addTask($groupId, $name, $scriptUrl, $hour = -1, $description = null, $enable = false);

    /**
     * Run a task now.
     *
     * @param $groupId
     * @param $name
     */
    public function runTask($groupId, $name);

    /**
     * Test if task exists.
     *
     * @param $groupId
     * @param $name
     */
    public function hasTask($groupId, $name);

    /**
     * Enable task of a flux.
     *
     * @param $groupId
     * @param $name
     */
    public function enableTask($groupId, $name);

    /**
     * Disable task for a flux.
     *
     * @param $groupId
     * @param $name
     */
    public function disableTask($groupId, $name);

    /**
     * Delete all tasks of a group.
     *
     * @param $groupId
     */
    public function deleteAllGroupTasks($groupId);
}
