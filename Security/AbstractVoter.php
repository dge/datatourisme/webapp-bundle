<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractVoter extends Voter
{
    /**
     * @var RoleHierarchy
     */
    private $roleHierarchy;

    /**
     * @param RoleHierarchy $roleHierarchy
     */
    public function setRoleHierarchy($roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        $reflection = new \ReflectionClass($this);
        $grantings = array_diff($reflection->getConstants(), $reflection->getParentClass()->getConstants());
        if (!in_array($attribute, $grantings, true)) {
            return false;
        }

        return true;
    }

    /**
     * hasRole.
     *
     * @param string        $role
     * @param UserInterface $user
     *
     * @return bool
     */
    public function hasRole($role, UserInterface $user)
    {
        if (!($role instanceof RoleInterface)) {
            $role = new Role($role);
        }
        foreach ($user->getRoles() as $userRole) {
            if (in_array($role, $this->roleHierarchy->getReachableRoles(array(new Role($userRole))))) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $token
     *
     * @return bool
     */
    public function isImpersonated(TokenInterface $token)
    {
        foreach ($token->getRoles() as $role) {
            if ('ROLE_PREVIOUS_ADMIN' == $role->getRole()) {
                return true;
            }
        }

        return false;
    }
}
