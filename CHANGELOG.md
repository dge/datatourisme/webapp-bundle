CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## En cours
=======

## [1.1.6] - 2019-12-12

### Modifications
- ajout d'une methode getRouteTitle aux notifications

## [1.1.5] - 2019-12-06

### Modifications
- ajout d'une methode isMailingGranted a MailerRecipientInterface pour gérer les inscription aux notifications

### Fixes
- Meilleur comportement responsif du header

## [1.1.4] - 2019-10-01

### Modifications
- lien hypetext : mise à jour de l'adresse du site datatourisme.fr en https://info.datatourisme.gouv.fr

## [1.1.3] - 2019-01-15

### Ajout
- Ajout de meta description 

## [1.1.2] - 2018-07-13

### Fixes
- Language switcher on error pages

## [1.1.1] - 2018-07-12

### Fixes
- Ajout de traductions communes
- Ajout d'un argument de langue au lien vers les mentions légales

## [1.1.0] - 2018-06-19

### Ajout
- Gestion des traductions pour l'ensemble des composants

=======

## [1.0.9] - 2018-06-06

### Fixes
- Fix MomentExtension to use the current request locale + locale map

=======

## [1.0.8] - 2018-05-25

### Fixes
- hotfix : notification.html.twig check extra.description is defined

=======

## [1.0.7] - 2018-05-16

### Fixes
- hotfix : notification twig environment : reactivate auto escape !

=======

## [1.0.6] - 2018-05-15

### Modifications
- notification : add description in template

=======

## [1.0.5] - 2018-05-14

### Correction
- MailerHandler : fix count() check (raise WARNING in php 7.2)

=======

## [1.0.4] - 2018-04-25

### Modifications
- CSVWriter : changement de methode de définition des champs
- Footer : Modification du lien vers l'espace Framagit

=======

## [1.0.3] - 2018-04-10

### Correction
- Nom du package !!

=======

## [1.0.2] - 2018-04-10

### Correction
- Correction de la version de moment

=======

## [1.0.1] - 2018-04-10

### Modifications
- Modification de liens dans le footer

### Ajout
- Ajout de la classe utilitaire CSVWriter

=======

## [1.0.0] - 2018-03-01

### Modifications
- Symfony 3.4
- Add versioning-bundle

=======

## [0.1.1] - 2017-07-04
### Correction
- Les menus se superposaient dans une certaine fourchette de breakpoints CSS
- Correction SASS et JS de bootstrap notify
- Fix Moment.php
- Fix Selectize
### Modifications
- Les fonts sont désormais chargées en local et non plus depuis les CDN
### Ajouts
- AuthentificationEntryPoint : renvoi d'un code 401 en cas de requête AJAX
- Composant JS frameloader
- UserRepository abstrait, incluant la method findByRole
- Service Mailer permettant de gérer l'envoi groupé de mails
- Service de notification permettant l'envoi par mail + enregistrement en db via un channel monolog
- Composant Modal : ajout de l'option modal-fh
- Intégration des service d'abstraction Rundeck + Beanstalk
- Ajout d'un service de formatter Monolog

=======

## [0.1.0] - 2017-04-18
### Ajouts
- Thème SASS DATAtourisme
- Thèmes TWIG divers
- Trait d'entités
    - Gestion d'upload de fichiers
- Éléments de formulaire
    - CurrentPassword : validation du mot de passe de l'utilsiateur actuellement connecté
    - DateRangePicker : Implémentation de Bootstrap Daterange picker
    - Honeypot : Implémentation de la technique de Honeybot contre les bots
    - ImagePreview : Champ d'upload d'image avec prévisualisation
    - RoleType : Champ de sélection de rôles limité aux rôles accessibles par l'utilisateur
    - StrongPassword : Champ de saisi de mot de passe compatible avec les contraintes de sécurité RGE
    - UpdatePassword : Champ combinant CurrentPassword et StrongPassword
- Voter de menu
    - UriPrefixVoter : utilise le début de l'URL pour déternier les éléments de menus actifs
- Pagination
    - Service de gestion de liste filtrés/triable
- Securité
    - Classe de Voter abstraite contenant les opérations utiles aux Voters des applications
- Composants Javascript :
    - Modal ajax
    - Bootstrap Datepicker
    - Boostrap Notify
    - ConditionalVisibility
    - ImageUploadPreview
    - Table filter + sortable
    - Tooltips
- Extensions TWIG :
    - MomentExtension : implémentation de Moment.php
    - UserExtension : hasRole avec gestion de la hiérarchie des rôles
    - WebAppExtension
- Validateurs :
    - StrongPassword : validation de mot de passe fort compatible avec les contraintes de sécurité RGE
