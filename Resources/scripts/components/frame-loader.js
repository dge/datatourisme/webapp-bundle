/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 

'use strict';

var $ = require("jquery");

$.fn.initComponents.add(function(dom) {
    $('[data-frameloader]', dom).each(function () {
        var self = $(this);
        var url = self.data('frameloader');
        var interval = self.data('interval');
        function update() {
            $.get(url, function (data) {
                self.html(data);
            }).always(function() {
                setTimeout(update, interval ? interval : 30000);
            });
        }
        update();
    });
});
