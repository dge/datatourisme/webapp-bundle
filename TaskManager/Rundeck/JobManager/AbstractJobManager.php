<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\JobManager;

use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\Connection\Project;
use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\Connection\RundeckClient;

/**
 * Class AbstractJobManager.
 */
abstract class AbstractJobManager implements JobManagerInterface
{
    /**
     * @var Project
     */
    private $project;

    /**
     * @var RundeckClient
     */
    private $client;

    /**
     * RundeckHelper constructor.
     *
     * @param Project       $project
     * @param RundeckClient $client
     */
    public function __construct(Project $project, RundeckClient $client)
    {
        $this->project = $project;
        $this->client = $client;
    }

    /**
     * Schedule or update a task.
     *
     * @param string     $name
     * @param string     $description
     * @param int|string $groupId
     * @param string     $scriptUrl
     * @param int        $hour
     * @param bool       $enable
     *
     * @return int
     *
     * @throws \Exception
     */
    public function addTask($groupId, $name, $scriptUrl, $hour = -1, $description = null, $enable = false)
    {
        $hour = (is_numeric($hour)) ? $hour : -1;
        // random hour not stored in database
        $hour = ($hour === -1) ? rand(0, 6) : $hour;

        $job = $this->getJob($groupId, $name, false);
        $mode = 'create';
        if (!$job) {
            $job = new Job($this->client);
        } else {
            $mode = 'update';
        }

        $job->setName($name);
        $job->setDescription($description);
        $job->setGroup($this->getRundeckGroup($groupId));
        $job->addScriptUrl($scriptUrl);
        $job->addSchedule('0 0 '.$hour.' ? * * *');
        $job->setExecutionEnabled($enable);
        $jobRundeck = $this->project->addJob($job, $mode);
        $job->setId($jobRundeck->id);

        return $job->getId();
    }

    /**
     * Run a task now.
     *
     * @param $groupId
     * @param $name
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function runTask($groupId, $name)
    {
        return $this->getJob($groupId, $name)->run();
    }

    /**
     * Test if task exists.
     *
     * @param $groupId
     * @param $name
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function hasTask($groupId, $name)
    {
        $job = $this->getJob($groupId, $name, false);

        return null !== $job;
    }

    /**
     * Enable task of a flux.
     *
     * @param $groupId
     * @param $name
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function enableTask($groupId, $name)
    {
        return $this->getJob($groupId, $name)->enable();
    }

    /**
     * Disable task for a flux.
     *
     * @param $groupId
     * @param $name
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function disableTask($groupId, $name)
    {
        return $this->getJob($groupId, $name)->disable();
    }

    /**
     * Delete all tasks of a group.
     *
     * @param $groupId
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function deleteAllGroupTasks($groupId)
    {
        return $this->project->deleteAllJobs($this->getRundeckGroup($groupId));
    }

    /**
     * Define a group name strategy based on groupId
     *  like producteur/flux-{groupId} for example.
     *
     * @param $groupId
     *
     * @return string
     */
    abstract protected function getRundeckGroup($groupId);

    /**
     * @param $groupId
     * @param $name
     * @param $error
     *
     * @return Job
     *
     * @throws \Exception
     */
    protected function getJob($groupId, $name, $error = true)
    {
        $group = $this->getRundeckGroup($groupId);
        $job = $this->project->getJobFromList($group, $name);
        if ($error && !$job) {
            throw new \Exception('The rundeck task does not exist');
        }

        return $job;
    }
}
