<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Twig;

use Symfony\Component\Serializer\Normalizer\DataUriNormalizer;

/**
 * Class WebAppExtension.
 */
class WebAppExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('data_uri_normalize', array($this, 'dataUriNormalizeFilter')),
            new \Twig_SimpleFilter('file_name', array($this, 'fileNameFilter')),
            new \Twig_SimpleFilter('emailize', array($this, 'emailizeFilter')),
        );
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('get_static', array($this, 'getStatic')),
        );
    }

    /**
     * @param $file
     *
     * @return array|string|\Symfony\Component\Serializer\Normalizer\scalar
     */
    public function dataUriNormalizeFilter($file)
    {
        if ($file instanceof \SplFileInfo) {
            $file = $file->openFile();
        }
        $normalizer = new DataUriNormalizer();

        return $normalizer->normalize($file);
    }

    /**
     * @param $file
     *
     * @return string
     */
    public function fileNameFilter($file)
    {
        return basename($file);
    }

    /**
     * Get static param of a class.
     *
     * @param $class
     * @param $varName
     *
     * @return mixed
     */
    public function getStatic($class, $varName)
    {
        return $class::$$varName;
    }

    /**
     * @param string $str
     *
     * @return string
     */
    public function emailizeFilter(string $str): string
    {
        return preg_replace(
            '~([A-z0-9\._-]+\@[A-z0-9_-]+\.)([A-z0-9\_\-\.]{1,}[A-z])~',
            '<a href="mailto:$1$2">$1$2</a>',
            $str
        );
    }
}
