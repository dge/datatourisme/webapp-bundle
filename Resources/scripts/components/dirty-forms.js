/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 

'use strict';

var $ = require("jquery");
require("jquery.dirtyforms");

$.DirtyForms.dialog = {
    open: function (choice) {
        var $dialog = $('' +
            '<div class="modal fade" tabindex="-1" role="dialog">' +
            '   <div class="modal-dialog" role="document">' +
            '       <div class="modal-content">' +
            '           <div class="modal-header">' +
            '               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '               <h4 class="modal-title">Attention</h4>' +
            '           </div>' +
            '           <div class="modal-body">' +
            '               <p>Vous avez effectué des modifications. Si vous quittez cette page sans enregistrer, elles seront perdues.</p>' +
            '           </div>' +
            '           <div class="modal-footer">' +
            '               <button type="button" class="btn btn-default pull-left" data-dismiss="modal">' +
            '                   Rester ici' +
            '               </button>' +
            '               <button type="button" class="btn btn-primary" data-dismiss="modal">' +
            '                   Quitter cette page' +
            '               </button>' +
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '</div>');

        // Bind the events
        choice.bindEscKey = false;

        var onContinueClick = function () {
            choice.proceed = $.DirtyForms.choiceContinue = true;
        };
        var onHidden = function (e) {
            var commit = choice.isDF1 ? $.DirtyForms.choiceCommit : choice.commit;
            commit(e);
            if ($('body').data('df-dialog-appended') === true) {
                $dialog.remove();
            }
        };

        $dialog.find('.btn-primary').off('click', onContinueClick).on('click', onContinueClick);
        $dialog.off('hidden.bs.modal', onHidden).on('hidden.bs.modal', onHidden);

        // Show the dialog
        $dialog.modal({ show: true });
    }
};

$.fn.initComponents.add(function(dom) {
    $('.dirty-form', dom).each(function () {
        $(this).dirtyForms();
    });
});
