<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\Connection;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Cookie\SetCookie;

/**
 * Class RundeckClient.
 */
class RundeckClient
{
    /**
     * @var GuzzleClient
     */
    protected $client;

    /**
     * @var CookieJar
     */
    protected $cookie;

    /**
     * @var int
     */
    protected $majorMinorVersion;

    /**
     * RundeckClient constructor.
     *
     * @param $host
     * @param string $port
     * @param string $user
     * @param string $pass
     */
    public function __construct($host, $port = '4440', $user = '', $pass = '')
    {
        $this->setHost($host, $port);
        $this->setCredential($user, $pass);
    }

    /**
     * @param $host
     * @param string $port
     */
    public function setHost($host, $port = '4440')
    {
        $this->client = new GuzzleClient([
            'base_uri' => $host.':'.$port,
        ]);
    }

    /**
     * @param $user
     * @param $pass
     */
    public function setCredential($user, $pass)
    {
        $response = $this->request(
            'POST',
            '/j_security_check', [
                'allow_redirects' => false,
                'form_params' => [
                    'j_username' => $user,
                    'j_password' => $pass,
                ],
            ]
        );

        $setCookie = new SetCookie();
        $setCookie2 = $setCookie->fromString($response->getHeaders()['Set-Cookie'][0]);
        $setCookie2->setDomain('.');
        $this->cookie = new CookieJar();
        $this->cookie->setCookie($setCookie2);

        $resp = $this->request(
            'GET',
            '/api/1/system/info', [
                'cookies' => $this->cookie,
            ]
        );

        $version = (string) (simplexml_load_string($resp->getBody()->getContents())->system->rundeck->version);
        $version = explode('.', $version);
        $this->majorMinorVersion = (int) ($version[0].$version[1]);
    }

    /**
     * @param $method
     * @param string $uri
     * @param array  $options
     *
     * @return mixed
     */
    public function request($method, $uri = '', array $options = [])
    {
        return $this->getHttpClient()->request($method, $uri, $options);
    }

    /**
     * @return mixed
     */
    public function getHttpClient()
    {
        return $this->client;
    }

    /**
     * @param $client
     */
    public function setHttpClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getCookieClient()
    {
        return $this->cookie;
    }
}
