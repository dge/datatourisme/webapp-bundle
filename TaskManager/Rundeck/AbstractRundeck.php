<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck;

/**
 * Class AbstractRundeck.
 */
class AbstractRundeck
{
    /**
     * @param $resp
     *
     * @return mixed
     */
    protected function decodeResponse($resp)
    {
        $xml = simplexml_load_string($resp->getBody()->getContents());
        $json = json_encode($xml);

        return json_decode($json, true);
    }
}
