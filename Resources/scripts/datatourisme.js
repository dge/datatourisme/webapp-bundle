/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 

'use strict';

var $ = window.jQuery = window.$ = require('jquery');
require('bootstrap');

/** core **/
require("./core/app");

/** components **/
require("./components/modal");
require("./components/ajax-content");
require("./components/table-filters");
require("./components/table-sortable");
require("./components/image-upload-preview");
require("./components/bootstrap-notify");
require("./components/bootstrap-datepicker");
require("./components/tooltip");
require("./components/conditional-visibility");
require("./components/frame-loader");
require("./components/dirty-forms");
require("./components/debounce");

// init app
$(function($) {
    $("body").initComponents();
});

