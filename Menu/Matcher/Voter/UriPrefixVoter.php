<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Bundle\WebAppBundle\Menu\Matcher\Voter;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Voter based on the uri prefix.
 */
class UriPrefixVoter implements VoterInterface
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function matchItem(ItemInterface $item)
    {
        $pathInfo = $this->request->getPathInfo();

        if (null === $item->getUri()) {
            return null;
        }

        if ($item->getUri() === $pathInfo) {
            return true;
        }

        if ($prefixes = $item->getExtra('uri_prefix')) {
            // Check closure
            $matchPrefix = function (string $prefix) use (&$pathInfo): bool {
                return preg_match('/^'.preg_quote($prefix, '/').'/si', $pathInfo);
            };

            // String case
            if (is_string($prefixes)) {
                if (true === $matchPrefix($prefixes)) {
                    return true;
                }
            }

            // Array case
            if (is_array($prefixes)) {
                foreach ($prefixes as $prefix) {
                    if (true === $matchPrefix($prefix)) {
                        return true;
                    }
                }
            }
        }

        return null;
    }
}
